import { Plugins } from '@capacitor/core';
import { Component } from '@angular/core';
const { CameraPreview } = Plugins;
import { CameraPreviewOptions, CameraPreviewPictureOptions } from '@capacitor-community/camera-preview';
// import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';

// Needed for web registration
import '@capacitor-community/camera-preview';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  image = null;
  cameraActive = false;
  save = false;
  // torchActive = false;
  constructor(
    // private base64ToGallery: Base64ToGallery
  ) {}

  openCamera() {
    const cameraPreviewOptions: CameraPreviewOptions = {
      position: 'rear',
      parent: 'cameraPreview',
      className: 'cameraPreview'
    };
    CameraPreview.start(cameraPreviewOptions);
    this.cameraActive = true;
  }

  async stopCamera() {
    await CameraPreview.stop();
    this.cameraActive = false;
  }

  async captureImage() {
    const cameraPreviewPictureOptions: CameraPreviewPictureOptions = {
      quality: 90,
    };
    const result = await CameraPreview.capture(cameraPreviewPictureOptions);
    console.log(result);
    this.image = `data:image/jpeg;base64,${result.value}`;
    console.log(this.image);
    this.save = true;
    this.stopCamera();
  }

  flipCamera() {
    CameraPreview.flip();
  }

  saveImage() {
    if (this.save){
    console.log(this.image);
    this.save = false;
    }
  }
}
